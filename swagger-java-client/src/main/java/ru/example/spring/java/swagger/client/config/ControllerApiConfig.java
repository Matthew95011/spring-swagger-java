package ru.example.spring.java.swagger.client.config;

import org.openapitools.client.api.UserControllerApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ControllerApiConfig {

    @Bean
    public UserControllerApi controllerApi() {
        return new UserControllerApi();
    }

}
