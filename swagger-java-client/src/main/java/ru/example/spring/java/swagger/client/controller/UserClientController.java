package ru.example.spring.java.swagger.client.controller;

import org.openapitools.client.ApiException;
import org.openapitools.client.api.UserControllerApi;
import org.openapitools.client.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user/client")
public class UserClientController {

    private static final Logger logger = LoggerFactory.getLogger(UserClientController.class);

    private final UserControllerApi userControllerApi;

    public UserClientController(UserControllerApi userControllerApi) {
        this.userControllerApi = userControllerApi;
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable String id) throws ApiException {
        logger.info("Input param, id: {}", id);
        ResponseEntity<User> response = new ResponseEntity<>(userControllerApi.getUserById(id), HttpStatus.OK);
        logger.info("Response: {}", response);
        return response;
    }

}
