package ru.example.spring.java.swagger.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Address implements Serializable {
    private final static long serialVersionUID = 1L;

    private String uuid;
    private List<User> users;
    private String country;
    private String city;
    private String areaCode;
    private String street;
    private String houseNumber;
    private String apartmentNumber;

    public Address() {
    }

    public Address(String uuid, List<User> users, String country, String city, String areaCode, String street, String houseNumber, String apartmentNumber) {
        this.uuid = uuid;
        this.users = users;
        this.country = country;
        this.city = city;
        this.areaCode = areaCode;
        this.street = street;
        this.houseNumber = houseNumber;
        this.apartmentNumber = apartmentNumber;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(String apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(uuid, address.uuid) && Objects.equals(users, address.users) && Objects.equals(country, address.country) && Objects.equals(city, address.city) && Objects.equals(areaCode, address.areaCode) && Objects.equals(street, address.street) && Objects.equals(houseNumber, address.houseNumber) && Objects.equals(apartmentNumber, address.apartmentNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, users, country, city, areaCode, street, houseNumber, apartmentNumber);
    }

    @Override
    public String toString() {
        return "Address{" +
                "uuid='" + uuid + '\'' +
                ", users=" + users +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", areaCode='" + areaCode + '\'' +
                ", street='" + street + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", apartmentNumber='" + apartmentNumber + '\'' +
                '}';
    }
}
