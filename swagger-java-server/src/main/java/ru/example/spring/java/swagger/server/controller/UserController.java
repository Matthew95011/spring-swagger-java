package ru.example.spring.java.swagger.server.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.example.spring.java.swagger.model.User;
import ru.example.spring.java.swagger.server.service.UserService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{uuid}")
    @Operation(summary = "Get user by UUID", description = "Получение пользователя по UUID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success",content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))
            }),
            @ApiResponse(responseCode = "404", description = "Not Found", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))
            })
    })
    @Parameter(name = "uuid", example = "2a30807a-0aa6-4a6a-9437-6f9f63a60290", required = true)
    public ResponseEntity<User> getUserById(@PathVariable("uuid") String uuid) {
        return new ResponseEntity<>(userService.getUserbyId(uuid), HttpStatus.OK);
    }

    @GetMapping
    @Operation(summary = "Get all users", description = "Получение всех пользователей из БД")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = ArrayList.class))
            }),
            @ApiResponse(responseCode = "404", description = "Not Found", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = ArrayList.class))
            })
    })
    public ResponseEntity<List<User>> getAllUser() {
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

    @DeleteMapping("/{uuid}")
    @Operation(description = "Удаление пользователя по UUID")
    @Parameter(name = "uuid", example = "2a30807a-0aa6-4a6a-9437-6f9f63a60290", required = true)
    public void deleteById(@PathVariable("uuid") String uuid) {
        userService.deleteUser(uuid);
    }


}
