package ru.example.spring.java.swagger.server.service;

import org.springframework.stereotype.Service;
import ru.example.spring.java.swagger.model.User;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class UserService {

    private Map<String, User> dbUsers;

    public User getUserbyId(String uuid) {
        return dbUsers.get(uuid);
    }

    public List<User> getAllUsers() {
        return new ArrayList<>(dbUsers.values());
    }

    public void deleteUser(String uuid) {
        dbUsers.remove(uuid);
    }

    @PostConstruct
    private void init() {
        dbUsers = new HashMap<>();

        User user = new User();
        user.setUuid("2a30807a-0aa6-4a6a-9437-6f9f63a60290");
        user.setFirstName("FirstName" + 0);
        user.setLastName("LastName" + 0);
        user.setMiddleName("MiddleName" + 0);
        user.setAge(0);

        dbUsers.put(user.getUuid(), user);

        for (int i = 0; i < 20; i++) {
            user = new User();

            user.setUuid(UUID.randomUUID().toString());
            user.setFirstName("FirstName" + i);
            user.setLastName("LastName" + i);
            user.setMiddleName("MiddleName" + i);
            user.setAge(i);

            dbUsers.put(user.getUuid(), user);
        }
    }

}
