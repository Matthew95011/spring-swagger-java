package ru.example.spring.java.swagger.server.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI api() {
        return new OpenAPI().info(apiInfo());
    }

    private Info apiInfo() {
        return new Info()
                .title("")
                .description("")
                .version("")
                .contact(apiContact());

    }

    private Contact apiContact() {
        return new Contact()
                .email("matthew.test@mail.ru")
                .name("Matthew")
                .url("https://github.com/matthew95011");
    }

}
